#FROM maven:3.9.6-eclipse-temurin-11 as build

#RUN mkdir app 


#WORKDIR app

#RUN git clone https://gitlab.com/kesav38/maven-web-application.git

#RUN cd maven-web-application/ && mvn clean package


FROM tomcat:8.0.18-jre8

COPY target/maven-web-application.war /usr/local/tomcat/webapps/maven-web-application.war
